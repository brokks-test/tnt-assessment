//Gulp
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');


//Build our css form scss files
gulp.task('sass', function(){
	return gulp.src('src/scss/*.scss')
	.pipe(plugins.concat('style.scss'))
	.pipe(plugins.sass({
			outputStyle: 'compact',
			sourceComments: false
	})) //using gulp sass
	.pipe(gulp.dest('build/css/'))
	.pipe(bsserver.reload({
	 	stream:true
	 }))
});

// Clean up build directory
gulp.task('clean', function() {
	return del([
		'./build/**/*'
		]);
});

//Add watch for live tasks
gulp.task('watch',  function(){
	gulp.watch('src/scss/**/*', ['sass']);
	gulp.watch('src/*.html', ['useref', bsserver.reload]);

});

//Spinup local server for development
var bsserver = require('browser-sync').create();

gulp.task('browserSync', function() {
 	bsserver.init({
    server: {
      baseDir: 'build'
    },
  })
})

//Add correct link in our html files
gulp.task('useref', function(){
  return gulp.src('src/*.html')
    .pipe(plugins.useref())
    .pipe(gulp.dest('build'))
});

//copy fonts to build folder
gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
  .pipe(gulp.dest('build/fonts'))
})

gulp.task('default', function (callback) {
  runSequence('clean', 'sass' ,'fonts','useref', 'browserSync', 'watch',
    callback
  )
})